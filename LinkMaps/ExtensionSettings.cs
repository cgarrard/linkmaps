﻿// ----------------------------------------------------------------------------------------------------
// <copyright file="ExtensionSettings.cs" company="USU RS/GIS Laboratory">
//   Copyright (c) USU RS/GIS Laboratory. All rights reserved.
// </copyright>
// ----------------------------------------------------------------------------------------------------

namespace LinkMaps
{
    using System.Collections.Generic;
    using System.Globalization;

    using ESRI.ArcGIS.Geometry;

    /// <summary>
    /// Class to hold the extension settings.
    /// </summary>
    internal class ExtensionSettings
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtensionSettings"/> class.
        /// </summary>
        public ExtensionSettings()
        {
            this.Rasters = new List<string>();
            this.Vectors = new List<string>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the center of the displayed area.
        /// </summary>
        public IPoint Center { get; set; }

        /// <summary>
        /// Gets or sets the list of raster layer paths.
        /// </summary>
        public List<string> Rasters { get; set; }

        /// <summary>
        /// Gets or sets the display scale.
        /// </summary>
        public double Scale { get; set; }

        /// <summary>
        /// Gets or sets the list of vector layer paths.
        /// </summary>
        public List<string> Vectors { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Clears extension settings.
        /// </summary>
        public void Clear()
        {
            this.Scale = 0;
            this.Center = null;
            this.Rasters.Clear();
            this.Vectors.Clear();
        }

        /// <summary>
        /// Deserializes the extension data.
        /// </summary>
        /// <param name="info">
        /// Dictionary containing the extension info. This is obtained from the stream read from the
        /// mxd file.
        /// </param>
        public void Deserialize(Dictionary<string, string> info)
        {
            this.Center = new PointClass { X = double.Parse(info["X"]), Y = double.Parse(info["Y"]) };
            this.Scale = double.Parse(info["Scale"]);
            this.Rasters = new List<string>(info["Rasters"].Split('|'));
            this.Rasters.RemoveAll(string.IsNullOrEmpty);
            this.Vectors = new List<string>(info["Vectors"].Split('|'));
            this.Vectors.RemoveAll(string.IsNullOrEmpty);
        }

        /// <summary>
        /// Serializes the extension data.
        /// </summary>
        /// <returns>
        /// The Dictionary containing the extension info.
        /// </returns>
        public Dictionary<string, string> Serialize()
        {
            var info = new Dictionary<string, string>
                           {
                               { "X", this.Center.X.ToString(CultureInfo.InvariantCulture) },
                               { "Y", this.Center.Y.ToString(CultureInfo.InvariantCulture) },
                               { "Scale", this.Scale.ToString(CultureInfo.InvariantCulture) },
                               { "Rasters", string.Join("|", this.Rasters.ToArray()) },
                               { "Vectors", string.Join("|", this.Vectors.ToArray()) }
                           };
            return info;
        }

        #endregion
    }
}