﻿namespace LinkMaps
{
    partial class LinkMapsDockableWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LinkMapsDockableWindow));
            this.linkMapControl = new ESRI.ArcGIS.Controls.AxMapControl();
            this.axLicenseControl1 = new ESRI.ArcGIS.Controls.AxLicenseControl();
            this.addLayerButton = new System.Windows.Forms.Button();
            this.layersTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.clearButton = new System.Windows.Forms.Button();
            this.linkToolbarControl = new ESRI.ArcGIS.Controls.AxToolbarControl();
            this.msgLabel = new System.Windows.Forms.Label();
            this.removeLayersPanel = new System.Windows.Forms.Panel();
            this.closeLayersbutton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.removeLayersTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.linkMapControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axLicenseControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkToolbarControl)).BeginInit();
            this.removeLayersPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // linkMapControl
            // 
            this.linkMapControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.linkMapControl.Location = new System.Drawing.Point(0, 37);
            this.linkMapControl.Name = "linkMapControl";
            this.linkMapControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("linkMapControl.OcxState")));
            this.linkMapControl.Size = new System.Drawing.Size(400, 400);
            this.linkMapControl.TabIndex = 0;
            //this.linkMapControl.OnAfterDraw += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnAfterDrawEventHandler(this.linkMapControl_OnAfterDraw);
            // 
            // axLicenseControl1
            // 
            this.axLicenseControl1.Enabled = true;
            this.axLicenseControl1.Location = new System.Drawing.Point(63, 352);
            this.axLicenseControl1.Name = "axLicenseControl1";
            this.axLicenseControl1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axLicenseControl1.OcxState")));
            this.axLicenseControl1.Size = new System.Drawing.Size(32, 32);
            this.axLicenseControl1.TabIndex = 1;
            // 
            // addLayerButton
            // 
            this.addLayerButton.FlatAppearance.BorderSize = 0;
            this.addLayerButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.addLayerButton.Image = global::LinkMaps.Properties.Resources.DataAdd16;
            this.addLayerButton.Location = new System.Drawing.Point(3, 7);
            this.addLayerButton.Name = "addLayerButton";
            this.addLayerButton.Size = new System.Drawing.Size(24, 24);
            this.addLayerButton.TabIndex = 2;
            this.addLayerButton.UseVisualStyleBackColor = true;
            this.addLayerButton.Click += new System.EventHandler(this.AddLayerButtonClickEventHandler);
            // 
            // layersTableLayoutPanel
            // 
            this.layersTableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.layersTableLayoutPanel.AutoSize = true;
            this.layersTableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.layersTableLayoutPanel.ColumnCount = 1;
            this.layersTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layersTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layersTableLayoutPanel.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.AddColumns;
            this.layersTableLayoutPanel.Location = new System.Drawing.Point(397, 0);
            this.layersTableLayoutPanel.Name = "layersTableLayoutPanel";
            this.layersTableLayoutPanel.RowCount = 1;
            this.layersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.layersTableLayoutPanel.Size = new System.Drawing.Size(0, 0);
            this.layersTableLayoutPanel.TabIndex = 3;
            // 
            // clearButton
            // 
            this.clearButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.clearButton.Image = global::LinkMaps.Properties.Resources.LayerRemove16;
            this.clearButton.Location = new System.Drawing.Point(26, 7);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(24, 24);
            this.clearButton.TabIndex = 4;
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.ClearButtonClickEventHandler);
            // 
            // linkToolbarControl
            // 
            this.linkToolbarControl.Location = new System.Drawing.Point(50, 3);
            this.linkToolbarControl.Name = "linkToolbarControl";
            this.linkToolbarControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("linkToolbarControl.OcxState")));
            this.linkToolbarControl.Size = new System.Drawing.Size(265, 28);
            this.linkToolbarControl.TabIndex = 5;
            // 
            // msgLabel
            // 
            this.msgLabel.AutoSize = true;
            this.msgLabel.BackColor = System.Drawing.Color.White;
            this.msgLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msgLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.msgLabel.Location = new System.Drawing.Point(140, 137);
            this.msgLabel.Name = "msgLabel";
            this.msgLabel.Size = new System.Drawing.Size(81, 20);
            this.msgLabel.TabIndex = 6;
            this.msgLabel.Text = "message";
            this.msgLabel.Visible = false;
            // 
            // removeLayersPanel
            // 
            this.removeLayersPanel.AutoSize = true;
            this.removeLayersPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.removeLayersPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.removeLayersPanel.Controls.Add(this.closeLayersbutton);
            this.removeLayersPanel.Controls.Add(this.label1);
            this.removeLayersPanel.Controls.Add(this.removeLayersTableLayoutPanel);
            this.removeLayersPanel.Location = new System.Drawing.Point(43, 99);
            this.removeLayersPanel.Name = "removeLayersPanel";
            this.removeLayersPanel.Size = new System.Drawing.Size(297, 232);
            this.removeLayersPanel.TabIndex = 7;
            this.removeLayersPanel.Visible = false;
            // 
            // closeLayersbutton
            // 
            this.closeLayersbutton.FlatAppearance.BorderSize = 0;
            this.closeLayersbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.closeLayersbutton.Location = new System.Drawing.Point(244, 4);
            this.closeLayersbutton.Name = "closeLayersbutton";
            this.closeLayersbutton.Size = new System.Drawing.Size(48, 19);
            this.closeLayersbutton.TabIndex = 2;
            this.closeLayersbutton.Text = "Close";
            this.closeLayersbutton.UseVisualStyleBackColor = true;
            this.closeLayersbutton.Click += new System.EventHandler(this.CloseLayersButtonClickEventHandler);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Click on a layer to remove it";
            // 
            // removeLayersTableLayoutPanel
            // 
            this.removeLayersTableLayoutPanel.ColumnCount = 1;
            this.removeLayersTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.removeLayersTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.removeLayersTableLayoutPanel.Location = new System.Drawing.Point(5, 50);
            this.removeLayersTableLayoutPanel.Name = "removeLayersTableLayoutPanel";
            this.removeLayersTableLayoutPanel.RowCount = 2;
            this.removeLayersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.removeLayersTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.removeLayersTableLayoutPanel.Size = new System.Drawing.Size(287, 177);
            this.removeLayersTableLayoutPanel.TabIndex = 0;
            // 
            // LinkMapsDockableWindow
            // 
            this.Controls.Add(this.removeLayersPanel);
            this.Controls.Add(this.msgLabel);
            this.Controls.Add(this.linkToolbarControl);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.layersTableLayoutPanel);
            this.Controls.Add(this.addLayerButton);
            this.Controls.Add(this.axLicenseControl1);
            this.Controls.Add(this.linkMapControl);
            this.Name = "LinkMapsDockableWindow";
            this.Size = new System.Drawing.Size(400, 437);
            ((System.ComponentModel.ISupportInitialize)(this.linkMapControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axLicenseControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkToolbarControl)).EndInit();
            this.removeLayersPanel.ResumeLayout(false);
            this.removeLayersPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ESRI.ArcGIS.Controls.AxMapControl linkMapControl;
        private ESRI.ArcGIS.Controls.AxLicenseControl axLicenseControl1;
        private System.Windows.Forms.Button addLayerButton;
        private System.Windows.Forms.TableLayoutPanel layersTableLayoutPanel;
        private System.Windows.Forms.Button clearButton;
        private ESRI.ArcGIS.Controls.AxToolbarControl linkToolbarControl;
        private System.Windows.Forms.Label msgLabel;
        private System.Windows.Forms.Panel removeLayersPanel;
        private System.Windows.Forms.TableLayoutPanel removeLayersTableLayoutPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button closeLayersbutton;





    }
}
