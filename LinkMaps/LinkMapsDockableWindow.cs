﻿// ----------------------------------------------------------------------------------------------------
// <copyright file="LinkMapsDockableWindow.cs" company="USU RS/GIS Laboratory">
//   Copyright (c) USU RS/GIS Laboratory. All rights reserved.
// </copyright>
// ----------------------------------------------------------------------------------------------------
namespace LinkMaps
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows.Forms;

    using ESRI.ArcGIS.ArcMapUI;
    using ESRI.ArcGIS.Carto;
    using ESRI.ArcGIS.Catalog;
    using ESRI.ArcGIS.CatalogUI;
    using ESRI.ArcGIS.Controls;
    using ESRI.ArcGIS.DataSourcesFile;
    using ESRI.ArcGIS.DataSourcesRaster;
    using ESRI.ArcGIS.Desktop.AddIns;
    using ESRI.ArcGIS.Display;
    using ESRI.ArcGIS.esriSystem;
    using ESRI.ArcGIS.Geodatabase;
    using ESRI.ArcGIS.Geometry;
    using ESRI.ArcGIS.SystemUI;

    using Button = System.Windows.Forms.Button;
    using Path = System.IO.Path;

    /// <summary>
    /// Designer class of the dockable window add-in. It contains user interfaces that
    /// make up the dockable window.
    /// </summary>
    public partial class LinkMapsDockableWindow : UserControl
    {
        #region Constants

        /// <summary>
        /// Used for checking floating point equality.
        /// </summary>
        private const double Epsilon = 1E-12;

        #endregion

        #region Fields

        /// <summary>
        /// The extension object.
        /// </summary>
        private LinkMapsExtension extension;

        /// <summary>
        /// The map object.
        /// </summary>
        private IMap map;

        /// <summary>
        /// Flag denoting whether or not we're currently syncing the map control and view.
        /// </summary>
        private bool syncing;

        /// <summary>
        /// The active view.
        /// </summary>
        private IActiveView view;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LinkMapsDockableWindow"/> class. Created by
        /// the Esri template.
        /// </summary>
        /// <param name="hook">
        /// The hook.
        /// </param>
        public LinkMapsDockableWindow(object hook)
        {
            this.InitializeComponent();
            this.Hook = hook;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the host object of the dockable window. Created by the Esri template.
        /// </summary>
        private object Hook { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The OnCreateControl handler for the dockable window. Runs when the window is created.
        /// </summary>
        protected override void OnCreateControl()
        {
            Debug.Print("OnCreateControl");
            base.OnCreateControl();

            // If we've already set the extension field, then we don't need to repeat any of this stuff.
            if (this.extension != null)
            {
                return;
            }

            try
            {
                Debug.Print("Initializing");
                this.extension = LinkMapsExtension.GetExtension();
                this.SetMap();

                // Create the toolbar.
                this.linkToolbarControl.SetBuddyControl(this.linkMapControl.Object);
                var tools = new List<string>
                                {
                                    "esriControls.ControlsMapZoomInTool", 
                                    "esriControls.ControlsMapZoomOutTool", 
                                    "esriControls.ControlsMapPanTool", 
                                    "esriControls.ControlsMapZoomToLastExtentBackCommand", 
                                    "esriControls.ControlsMapZoomToLastExtentForwardCommand", 
                                    "esriControls.ControlsMapFullExtentCommand"
                                };
                foreach (string tool in tools)
                {
                    this.linkToolbarControl.AddItem(
                        tool, -1, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
                }

                // Set up listeners.
                this.WireDocumentListeners();
                var viewEvents = (IActiveViewEvents_Event)this.map;
                viewEvents.ItemAdded += this.ViewItemAddedEventHandler;
                this.linkMapControl.OnAfterDraw += this.LinkMapControlAfterDrawEventHandler;

                // Load extension layers if there are any.
                this.LoadLayers();
            }
            catch (Exception ex)
            {
                this.ShowErrorMessage("Unable to initialize LinkMaps", ex);
            }
        }

        /// <summary>
        /// Click handler for the "Add Layer" button. Lets the user select a layer from disk and adds
        /// it to the map control and extension.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event.
        /// </param>
        private void AddLayerButtonClickEventHandler(object sender, EventArgs e)
        {
            try
            {
                // Set up the dialog and filters that determine the types of files the user can select.
                IGxObjectFilter rasterFilter = new GxFilterRasterDatasetsClass();
                IGxObjectFilter shapefileFilter = new GxFilterShapefilesClass();
                IGxDialog dlg = new GxDialogClass();
                var filters = (IGxObjectFilterCollection)dlg;
                filters.AddFilter(rasterFilter, false);
                filters.AddFilter(shapefileFilter, false);
                dlg.Title = "Load one or more datasets";
                dlg.ButtonCaption = "Load";
                dlg.AllowMultiSelect = true;

                // Let the user select layers. Return if they cancelled out instead.
                IEnumGxObject objects;
                if (!dlg.DoModalOpen(0, out objects))
                {
                    return;
                }

                // Loop through the list if selected layers and add them to the map control.
                IGxObject obj;
                while ((obj = objects.Next()) != null)
                {
                    var dataset = (IGxDataset)obj;
                    switch (dataset.Type)
                    {
                        case esriDatasetType.esriDTRasterDataset:
                            this.AddRasterLayer((IRasterDataset)dataset.Dataset, obj.FullName, true);
                            break;
                        case esriDatasetType.esriDTFeatureClass:
                            this.AddVectorLayer((IFeatureClass)dataset.Dataset, obj.FullName, true);
                            break;
                    }
                }

                // If we're already syncing or there aren't any layers in the map control then we don't
                // need to do anything else.
                if (this.syncing || this.map.LayerCount == 0)
                {
                    return;
                }

                // Make sure the map control has the same spatial reference as the active view, sync
                // the map control to the active view, and set up the listeners so we'll start syncing.
                this.linkMapControl.SpatialReference = this.map.SpatialReference;
                this.SaveViewSettings();
                this.SyncDockToExtension();
                this.WireActiveViewListeners();
            }
            catch (Exception ex)
            {
                this.ShowErrorMessage("Unable to add layer", ex);
            }
        }

        /// <summary>
        /// Adds a raster layer to the map control.
        /// </summary>
        /// <param name="path">
        /// The layer path on disk.
        /// </param>
        private void AddRasterLayer(string path)
        {
            IWorkspaceFactory factory = new RasterWorkspaceFactoryClass();
            var ws = (IRasterWorkspace)factory.OpenFromFile(Path.GetDirectoryName(path), 0);
            IRasterDataset dataset = ws.OpenRasterDataset(Path.GetFileName(path));
            this.AddRasterLayer(dataset, path, false);
        }

        /// <summary>
        /// Adds a raster layer to the map control. It's displayed with a min-max stretch of bands
        /// 5,4,2.
        /// </summary>
        /// <param name="rasterDataset">
        /// The raster dataset to make the layer from.
        /// </param>
        /// <param name="path">
        /// The raster path on disk.
        /// </param>
        /// <param name="addToExtension">
        /// True if the layer should be added to the extension (needed because it might already be
        /// there, like when loading an existing mxd).
        /// </param>
        private void AddRasterLayer(IRasterDataset rasterDataset, string path, bool addToExtension)
        {
            // Make sure the dataset has statistics and pyramids.
            this.PrepareRasterDataset(rasterDataset);

            // Create the layer.
            IRasterLayer rasterLayer = new RasterLayerClass();
            rasterLayer.CreateFromDataset(rasterDataset);

            // Make sure that the layer has enough bands so the renderer doesn't crash.
            if (rasterLayer.BandCount > 1 && rasterLayer.BandCount < 5)
            {
                throw new Exception("Raster layers must have either one or at least 5 bands");
            }

            // Apply a 3-band renderer if needed.
            if (rasterLayer.BandCount > 1)
            {
                IRasterRGBRenderer2 rasterRenderer = new RasterRGBRendererClass();
                rasterRenderer.SetBandIndices(4, 3, 1);
                rasterRenderer.UseAlphaBand = false;
                ((IRasterStretch2)rasterRenderer).StretchType =
                    esriRasterStretchTypesEnum.esriRasterStretch_MinimumMaximum;
                rasterLayer.Renderer = (IRasterRenderer)rasterRenderer;
            }

            // Add the layer to the map control underneath any vector layers.
            this.linkMapControl.AddLayer(rasterLayer, this.extension.VectorCount);

            // Create a radio button for switching raster layer visibility.
            var rb = new RadioButton { Tag = rasterLayer, Text = rasterLayer.Name, AutoSize = true };
            rb.CheckedChanged += this.LayerRadioButtonCheckedChangedEventHandler;
            this.layersTableLayoutPanel.Controls.Add(rb);
            rb.Checked = true;

            // Create a button for removing the layer from the map control.
            var btn = new Button { Text = rasterLayer.Name, AutoSize = true };
            btn.Tag = new LayerInfo(true, path, rasterLayer, btn.Text);
            btn.Click += this.LayerButtonClickEventHandler;
            this.removeLayersTableLayoutPanel.Controls.Add(btn);

            // Add the layer to the extension if needed.
            if (addToExtension)
            {
                this.extension.AddRaster(path);
            }
        }

        /// <summary>
        /// Adds a polygon vector layer to the map control.
        /// </summary>
        /// <param name="path">
        /// The layer path on disk.
        /// </param>
        private void AddVectorLayer(string path)
        {
            IWorkspaceFactory factory = new ShapefileWorkspaceFactoryClass();
            var ws = (IFeatureWorkspace)factory.OpenFromFile(Path.GetDirectoryName(path), 0);
            IFeatureClass fc = ws.OpenFeatureClass(Path.GetFileNameWithoutExtension(path));
            this.AddVectorLayer(fc, path, false);
        }

        /// <summary>
        /// Adds a polygon vector layer to the map control. It's displayed with a yellow outline and
        /// labels features with the FID.
        /// </summary>
        /// <param name="featureClass">
        /// The feature class to create the layer from.
        /// </param>
        /// <param name="path">
        /// The path on disk.
        /// </param>
        /// <param name="addToExtension">
        /// True if the layer should be added to the extension (needed because it might already be
        /// there, like when loading an existing mxd).
        /// </param>
        private void AddVectorLayer(IFeatureClass featureClass, string path, bool addToExtension)
        {
            // Check that it is a polygon layer.
            if (featureClass.ShapeType != esriGeometryType.esriGeometryPolygon)
            {
                throw new Exception("Only polygon feature layers are supported at this time");
            }

            // Create a polygon renderer.
            IRgbColor yellow = new RgbColorClass();
            yellow.Red = 255;
            yellow.Green = 255;
            yellow.Blue = 0;
            ILineSymbol lineSymbol = new SimpleLineSymbolClass();
            lineSymbol.Color = yellow;
            ISimpleFillSymbol fillSymbol = new SimpleFillSymbolClass();
            fillSymbol.Style = esriSimpleFillStyle.esriSFSHollow;
            fillSymbol.Outline = lineSymbol;
            ISimpleRenderer shapefileRenderer = new SimpleRendererClass();
            shapefileRenderer.Symbol = (ISymbol)fillSymbol;

            // Create the layer and set the renderer.
            IFeatureLayer featureLayer = new FeatureLayerClass();
            featureLayer.FeatureClass = featureClass;
            var geoLayer = (IGeoFeatureLayer)featureLayer;
            geoLayer.Renderer = (IFeatureRenderer)shapefileRenderer;

            // Set up the labeling.
            geoLayer.DisplayAnnotation = true;
            IAnnotateLayerProperties labelProperties;
            IElementCollection placed;
            IElementCollection unplaced;
            geoLayer.AnnotationProperties.QueryItem(0, out labelProperties, out placed, out unplaced);
            var engineProperties = (ILabelEngineLayerProperties)labelProperties;
            engineProperties.Symbol.Color = yellow;
            engineProperties.Expression = "[FID]";

            // Add the layer to the map control.
            this.linkMapControl.AddLayer(featureLayer);

            // Create a button for removing the layer from the map control.
            var btn = new Button { Text = ((IDataset)featureLayer).BrowseName, AutoSize = true };
            btn.Tag = new LayerInfo(false, path, featureLayer, btn.Text);
            btn.Click += this.LayerButtonClickEventHandler;
            this.removeLayersTableLayoutPanel.Controls.Add(btn);

            // Add the layer to the extension if needed.
            if (addToExtension)
            {
                this.extension.AddVector(path);
            }
        }

        /// <summary>
        /// Click handler for the "Clear" button. Toggles the panel containing the buttons to remove
        /// layers.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event.
        /// </param>
        private void ClearButtonClickEventHandler(object sender, EventArgs e)
        {
            this.removeLayersPanel.Visible = !this.removeLayersPanel.Visible;
        }

        /// <summary>
        /// Click handler for the "Close" button. Hides the panel containing the buttons to remove
        /// layers.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event.
        /// </param>
        private void CloseLayersButtonClickEventHandler(object sender, EventArgs e)
        {
            this.removeLayersPanel.Visible = false;
        }

        /// <summary>
        /// The CloseDocument handler. Clears extension settings and removes everything from the
        /// map control and dockable window.
        /// </summary>
        private void DocumentCloseEventHandler()
        {
            Debug.Print("documentEvents_CloseDocument");
            this.extension.Clear();
            this.linkMapControl.ClearLayers();
            this.layersTableLayoutPanel.Controls.Clear();
            this.removeLayersTableLayoutPanel.Controls.Clear();
            this.UnWireActiveViewListeners();
            this.map = null;
            this.view = null;
        }

        /// <summary>
        /// The NewDocument handler. Grabs the current focus map and active view.
        /// </summary>
        private void DocumentNewEventHandler()
        {
            Debug.Print("documentEvents_NewDocument");
            this.SetMap();
        }

        /// <summary>
        /// The OpenDocument handler. Grabs the current focus map and active view, and loads any
        /// extension layers saved in the document.
        /// </summary>
        private void DocumentOpenEventHandler()
        {
            Debug.Print("documentEvents_OpenDocument");
            this.SetMap();
            try
            {
                this.LoadLayers();
            }
            catch (Exception ex)
            {
                this.ShowErrorMessage("Unable to load LinkMaps data", ex);
            }
        }

        /// <summary>
        /// Gets a raster radio button.
        /// </summary>
        /// <param name="name">
        /// The name of the raster.
        /// </param>
        /// <returns>
        /// The corresponding radio button.
        /// </returns>
        private Control GetRadioButton(string name)
        {
            return this.layersTableLayoutPanel.Controls.Cast<Control>().First(ctrl => ctrl.Text == name);
        }

        /// <summary>
        /// Gets the index of a raster layer in the map control.
        /// </summary>
        /// <param name="name">
        /// The name of the layer.
        /// </param>
        /// <returns>
        /// The index of the layer in the map control.
        /// </returns>
        private int GetRasterLayerIndex(string name)
        {
            for (int i = 0; i < this.linkMapControl.LayerCount; i++)
            {
                if (this.linkMapControl.get_Layer(i).Name == name)
                {
                    return i;
                }
            }

            return -1;
        }

        /// <summary>
        /// Gets the index of a vector layer in the map control.
        /// </summary>
        /// <param name="name">
        /// The name of the layer.
        /// </param>
        /// <returns>
        /// The index of the layer in the map control.
        /// </returns>
        private int GetVectorLayerIndex(string name)
        {
            for (int i = 0; i < this.linkMapControl.LayerCount; i++)
            {
                if (((IDataset)this.linkMapControl.get_Layer(i)).BrowseName == name)
                {
                    return i;
                }
            }

            return -1;
        }

        /// <summary>
        /// The Click handler for the buttons used to remove layers from the map control.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event.
        /// </param>
        private void LayerButtonClickEventHandler(object sender, EventArgs e)
        {
            try
            {
                var btn = (Button)sender;
                var info = (LayerInfo)btn.Tag;

                // Delete the layer from the map control and the extension. If it's a raster layer, 
                // also delete the radio button.
                if (info.IsRaster)
                {
                    this.linkMapControl.DeleteLayer(this.GetRasterLayerIndex(info.Name));
                    this.layersTableLayoutPanel.Controls.Remove(this.GetRadioButton(info.Name));
                    this.extension.DeleteRaster(info.Path);
                }
                else
                {
                    this.linkMapControl.DeleteLayer(this.GetVectorLayerIndex(info.Name));
                    this.extension.DeleteVector(info.Path);
                }

                // Remove the button and close the panel.
                this.removeLayersTableLayoutPanel.Controls.Remove(btn);
                this.removeLayersPanel.Visible = false;

                // If there are still layers in the extension, then we don't need to do anything else.
                if (this.extension.LayerCount > 0)
                {
                    return;
                }

                // If we were syncing, but now we're out of layers, get rid of the listeners on the
                // active view.
                if (this.syncing)
                {
                    this.UnWireActiveViewListeners();
                }
            }
            catch (Exception ex)
            {
                this.ShowErrorMessage("Unable to remove layer", ex);
            }
        }

        /// <summary>
        /// The CheckedChanged handler for the raster radio buttons. Makes the corresponding raster
        /// layer visible if the radio button is checked.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event.
        /// </param>
        private void LayerRadioButtonCheckedChangedEventHandler(object sender, EventArgs e)
        {
            var rb = (RadioButton)sender;
            ((IRasterLayer)rb.Tag).Visible = rb.Checked;
            this.linkMapControl.Refresh();
        }

        /// <summary>
        /// The OnAfterDraw handler for the map control. Make sure that the active view and map
        /// control are in sync.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event.
        /// </param>
        private void LinkMapControlAfterDrawEventHandler(
            object sender, IMapControlEvents2_OnAfterDrawEvent e)
        {
            // Don't need to do anything if we're in the wrong draw phase.
            if ((esriViewDrawPhase)e.viewDrawPhase != esriViewDrawPhase.esriViewInitialized)
            {
                return;
            }

            Debug.Print("linkMapControl_OnAfterDraw");

            // Get center and scale for the map control.
            IPoint center = ((IArea)this.linkMapControl.Extent).Centroid;
            double scale = this.linkMapControl.MapScale;

            // We don't need to do anything if the map control already matches the extension because
            // it means we were drawing in order to match it.
            if (this.SyncedToExtension(center, scale))
            {
                return;
            }

            // If we don't match the extension they we must be drawing in response to user input, so
            // we need to save the center and scale to the extension and sync the active view to these
            // values if we're currently syncing. The settings are saved even if not syncing, just in
            // case the user saves the mxd with nothing in the active view.
            this.SetExtensionSettings(center, scale);
            if (this.syncing)
            {
                this.SyncViewToExtension();
            }
        }

        /// <summary>
        /// Loads extension layers into the map control.
        /// </summary>
        private void LoadLayers()
        {
            Debug.Print("LoadLayers");

            // Don't need to do anything if there aren't any layers.
            if (this.extension.LayerCount == 0)
            {
                return;
            }

            // Add the vector layers (in reverse order so that the correct ones are on top).
            for (int i = this.extension.VectorCount; i > 0; i--)
            {
                this.AddVectorLayer(this.extension.Vectors[i - 1]);
            }

            // Add the raster layers (in reverse order so that the correct ones are on top).
            for (int i = this.extension.RasterCount; i > 0; i--)
            {
                this.AddRasterLayer(this.extension.Rasters[i - 1]);
            }

            // If there are layers in the active view, then sync the map control to the active view and
            // turn on listeners. Otherwise, just sync the map control to the settings in the saved 
            // extension.
            if (this.map.LayerCount > 0)
            {
                this.SaveViewSettings();
                this.SyncDockToExtension();
                this.WireActiveViewListeners();
            }
            else
            {
                this.SyncDockToExtension();
            }
        }

        /// <summary>
        /// Makes sure a raster dataset has statistics and pyramids so it can be shown in the map
        /// control.
        /// </summary>
        /// <param name="dataset">
        /// The raster dataset to check.
        /// </param>
        private void PrepareRasterDataset(IRasterDataset dataset)
        {
            this.Cursor = Cursors.WaitCursor;

            // Calculate statistics for each band if needed.
            var bands = (IRasterBandCollection)dataset;
            for (int i = 0; i < bands.Count; i++)
            {
                IRasterBand band = bands.Item(i);
                bool hasStats;
                band.HasStatistics(out hasStats);
                if (!hasStats)
                {
                    this.SetMessage(string.Format("Calculating statistics for band {0}...", i + 1));
                    band.ComputeStatsAndHist();
                }
            }

            // Build pyramids if needed.
            var pyramids = (IRasterPyramid)dataset;
            if (!pyramids.Present)
            {
                this.SetMessage("Building pyramids...");
                pyramids.Create();
            }

            this.SetMessage(null);
            this.Cursor = Cursors.Default;
        }

        /// <summary>
        /// Saves the displayed center and scale for the dockable window to the extension.
        /// </summary>
        private void SaveDockSettings()
        {
            Debug.Print("SaveDockSettings");
            this.SetExtensionSettings(
                ((IArea)this.linkMapControl.Extent).Centroid, this.linkMapControl.MapScale);
        }

        /// <summary>
        /// Saves the displayed center and scale for the active view to the extension.
        /// </summary>
        private void SaveViewSettings()
        {
            Debug.Print("SaveViewSettings");
            this.SetExtensionSettings(((IArea)this.view.Extent).Centroid, this.map.MapScale);
        }

        /// <summary>
        /// Saves a center and scale to the extension.
        /// </summary>
        /// <param name="center">
        /// The center point.
        /// </param>
        /// <param name="scale">
        /// The map display scale.
        /// </param>
        private void SetExtensionSettings(IPoint center, double scale)
        {
            this.extension.Center = center;
            this.extension.Scale = scale;
        }

        /// <summary>
        /// Sets the focus map and active view to instance variables for convenient referencing.
        /// </summary>
        private void SetMap()
        {
            Debug.Print("SetMap");
            this.map = ArcMap.Document.FocusMap;
            this.view = ArcMap.Document.ActiveView;
        }

        /// <summary>
        /// Displays a message on the dockable window.
        /// </summary>
        /// <param name="msg">
        /// The message to display, or null to clear.
        /// </param>
        private void SetMessage(string msg)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                this.msgLabel.Text = msg;
                this.msgLabel.Visible = true;
            }
            else
            {
                this.msgLabel.Visible = false;
            }

            this.Refresh();
        }

        /// <summary>
        /// Show an error message to the user.
        /// </summary>
        /// <param name="message">
        /// The message to show.
        /// </param>
        /// <param name="e">
        /// </param>
        /// An optional exception whose message will be appended to the user-provided message.
        private void ShowErrorMessage(string message, Exception e = null)
        {
            if (e != null)
            {
                message = string.Format("{0}:\n{1}", message, e.Message);
            }

            MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        /// Syncs the center and display scale of the dockable window to the values stored in the
        /// extension.
        /// </summary>
        private void SyncDockToExtension()
        {
            Debug.Print("SyncDockToExtension");
            this.linkMapControl.CenterAt(this.extension.Center);
            this.linkMapControl.MapScale = this.extension.Scale;
        }

        /// <summary>
        /// Syncs the center and display scale of the active view to the values stored in the
        /// extension.
        /// </summary>
        private void SyncViewToExtension()
        {
            Debug.Print("SyncViewToExtension");
            IEnvelope extent = this.view.Extent.Envelope;
            extent.CenterAt(this.extension.Center);
            this.view.Extent = extent;
            this.map.MapScale = this.extension.Scale;
            this.view.Refresh();
        }

        /// <summary>
        /// Checks to see if a center point and display scale are the same as the values stored in the
        /// extension.
        /// </summary>
        /// <param name="center">
        /// The center point to check.
        /// </param>
        /// <param name="scale">
        /// The display scale to check.
        /// </param>
        /// <returns>
        /// True if they match, false if not.
        /// </returns>
        private bool SyncedToExtension(IPoint center, double scale)
        {
            return (Math.Abs(scale - this.extension.Scale) < Epsilon) && (this.extension.Center != null)
                   && ((IClone)center).IsEqual((IClone)this.extension.Center);
        }

        /// <summary>
        /// Removes listeners on the active view.
        /// </summary>
        private void UnWireActiveViewListeners()
        {
            Debug.Print("UnwireActiveViewListeners");
            var viewEvents = (IActiveViewEvents_Event)this.map;
            viewEvents.AfterDraw -= this.ViewAfterDrawEventHandler;
            viewEvents.SpatialReferenceChanged -= this.ViewSpatialReferenceChangedEventHandler;
            viewEvents.ItemDeleted -= this.ViewItemDeletedEventHandler;
            this.syncing = false;
        }

        /// <summary>
        /// Removes listeners on the document.
        /// </summary>
        private void UnWireDocumentListeners()
        {
            Debug.Print("UnwireDocumentListeners");
            var documentEvents = (IDocumentEvents_Event)ArcMap.Document;
            documentEvents.OpenDocument -= this.DocumentOpenEventHandler;
            documentEvents.CloseDocument -= this.DocumentCloseEventHandler;
            documentEvents.NewDocument -= this.DocumentNewEventHandler;
        }

        /// <summary>
        /// The AfterDraw event handler for the active view.  Make sure that the active view and map
        /// control are in sync.
        /// </summary>
        /// <param name="display">
        /// The display.
        /// </param>
        /// <param name="phase">
        /// The drawing phase.
        /// </param>
        private void ViewAfterDrawEventHandler(IDisplay display, esriViewDrawPhase phase)
        {
            // Don't need to do anything if we're in the wrong draw phase.
            if (phase != esriViewDrawPhase.esriViewInitialized)
            {
                return;
            }

            Debug.Print("viewEvents_AfterDraw");

            // Get center and scale for the active view.
            IPoint center = ((IArea)this.view.Extent).Centroid;
            double scale = this.map.MapScale;

            // We don't need to do anything if the active view already matches the extension because
            // it means we were drawing in order to match it.
            if (this.SyncedToExtension(center, scale))
            {
                return;
            }

            // If we don't match the extension they we must be drawing in response to user input, so
            // we need to save the center and scale to the extension and sync the map control to these
            // values.
            this.SetExtensionSettings(center, scale);
            this.SyncDockToExtension();
        }

        /// <summary>
        /// The ItemAdded handler for the active view. Runs when the user adds a layer to the view.
        /// </summary>
        /// <param name="item">
        /// The item the user added.
        /// </param>
        private void ViewItemAddedEventHandler(object item)
        {
            Debug.Print("viewEvents_ItemAdded");

            // If we're already syncing or there is nothing in the map control, then we don't need to
            // do anything new.
            if (this.syncing || this.extension.LayerCount == 0)
            {
                return;
            }

            // Otherwise we need to start syncing, so we sync the active view to the map control since
            // it already had layers.
            this.SaveDockSettings();
            this.SyncViewToExtension();
            this.WireActiveViewListeners();
        }

        /// <summary>
        /// The ItemDeleted handler for the active view. Runs when the user removes a layer from the
        /// view.
        /// </summary>
        /// <param name="item">
        /// The item the user removed.
        /// </param>
        private void ViewItemDeletedEventHandler(object item)
        {
            Debug.Print("viewEvents_ItemDeleted");

            // If there aren't any layers left in the active view, but we're syncing, then stop it.
            if (this.map.LayerCount == 0 && this.syncing)
            {
                this.UnWireActiveViewListeners();
            }
        }

        /// <summary>
        /// The SpatialReferenceChanged handler for the active view. Runs when the user changes the
        /// spatial reference for the view. Just makes sure the map control matches.
        /// </summary>
        private void ViewSpatialReferenceChangedEventHandler()
        {
            this.linkMapControl.SpatialReference = this.map.SpatialReference;
        }

        /// <summary>
        /// Sets listeners on the active view.
        /// </summary>
        private void WireActiveViewListeners()
        {
            Debug.Print("WireActiveViewListeners");
            var viewEvents = (IActiveViewEvents_Event)this.map;
            viewEvents.AfterDraw += this.ViewAfterDrawEventHandler;
            viewEvents.SpatialReferenceChanged += this.ViewSpatialReferenceChangedEventHandler;
            viewEvents.ItemDeleted += this.ViewItemDeletedEventHandler;
            this.syncing = true;
        }

        /// <summary>
        /// Sets listeners on the document.
        /// </summary>
        private void WireDocumentListeners()
        {
            Debug.Print("WireDocumentListeners");
            var documentEvents = (IDocumentEvents_Event)ArcMap.Document;
            documentEvents.OpenDocument += this.DocumentOpenEventHandler;
            documentEvents.CloseDocument += this.DocumentCloseEventHandler;
            documentEvents.NewDocument += this.DocumentNewEventHandler;
        }

        #endregion

        /// <summary>
        /// Implementation class of the dockable window add-in. It is responsible for creating and
        /// disposing the user interface class of the dockable window. Created by the Esri template.
        /// </summary>
        public class AddinImpl : DockableWindow
        {
            #region Fields

            /// <summary>
            /// The m_window.
            /// </summary>
            private LinkMapsDockableWindow m_windowUI;

            #endregion

            #region Methods

            /// <summary>
            /// The dispose.
            /// </summary>
            /// <param name="disposing">
            /// The disposing.
            /// </param>
            protected override void Dispose(bool disposing)
            {
                if (this.m_windowUI != null)
                {
                    this.m_windowUI.Dispose(disposing);
                }

                base.Dispose(disposing);
            }

            /// <summary>
            /// The on create child.
            /// </summary>
            /// <returns>
            /// The <see cref="IntPtr" />.
            /// </returns>
            protected override IntPtr OnCreateChild()
            {
                this.m_windowUI = new LinkMapsDockableWindow(this.Hook);
                return this.m_windowUI.Handle;
            }

            #endregion
        }
    }
}