﻿// ----------------------------------------------------------------------------------------------------
// <copyright file="LayerInfo.cs" company="USU RS/GIS Laboratory">
//   Copyright (c) USU RS/GIS Laboratory. All rights reserved.
// </copyright>
// ----------------------------------------------------------------------------------------------------

namespace LinkMaps
{
    using ESRI.ArcGIS.Carto;

    /// <summary>
    /// Class to hold info about a layer being used in the extension.
    /// </summary>
    internal class LayerInfo
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LayerInfo"/> class.
        /// </summary>
        /// <param name="isRaster">
        /// True if the layer is a raster, False if it's a vector.
        /// </param>
        /// <param name="path">
        /// The path to the file on disk.
        /// </param>
        /// <param name="layer">
        /// The actual ILayer object.
        /// </param>
        /// <param name="name">
        /// The name of the layer.
        /// </param>
        public LayerInfo(bool isRaster, string path, ILayer layer, string name)
        {
            this.IsRaster = isRaster;
            this.Path = path;
            this.Layer = layer;
            this.Name = name;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets a value indicating whether the layer is a raster.
        /// </summary>
        public bool IsRaster { get; private set; }

        /// <summary>
        /// Gets the layer object.
        /// </summary>
        public ILayer Layer { get; private set; }

        /// <summary>
        /// Gets the layer name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the path on disk.
        /// </summary>
        public string Path { get; private set; }

        #endregion
    }
}