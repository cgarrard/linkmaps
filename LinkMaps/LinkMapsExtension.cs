﻿// ----------------------------------------------------------------------------------------------------
// <copyright file="LinkMapsExtension.cs" company="USU RS/GIS Laboratory">
//   Copyright (c) USU RS/GIS Laboratory. All rights reserved.
// </copyright>
// ----------------------------------------------------------------------------------------------------
namespace LinkMaps
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Windows.Forms;

    using ESRI.ArcGIS.Desktop.AddIns;
    using ESRI.ArcGIS.esriSystem;
    using ESRI.ArcGIS.Framework;
    using ESRI.ArcGIS.Geometry;

    /// <summary>
    /// The link maps extension.
    /// </summary>
    public class LinkMapsExtension : Extension
    {
        #region Static Fields

        /// <summary>
        /// The extension object.
        /// </summary>
        private static LinkMapsExtension extension;

        #endregion

        #region Fields

        /// <summary>
        /// The extension settings.
        /// </summary>
        private readonly ExtensionSettings settings;

        /// <summary>
        /// The dockable window.
        /// </summary>
        private IDockableWindow dockWindow;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LinkMapsExtension" /> class.
        /// </summary>
        public LinkMapsExtension()
        {
            this.settings = new ExtensionSettings();
            extension = this;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the center of the displayed area.
        /// </summary>
        internal IPoint Center
        {
            get
            {
                return this.settings.Center;
            }

            set
            {
                this.settings.Center = value;
            }
        }

        /// <summary>
        /// Gets the dockable window.
        /// </summary>
        /// <returns>
        /// The <see cref="IDockableWindow" />.
        /// </returns>
        internal IDockableWindow DockableWindow
        {
            get
            {
                if (this.dockWindow == null)
                {
                    UID dockUID = new UIDClass();
                    dockUID.Value = ThisAddIn.IDs.LinkMapsDockableWindow;
                    this.dockWindow = ArcMap.DockableWindowManager.GetDockableWindow(dockUID);
                }

                return this.dockWindow;
            }
        }

        /// <summary>
        /// Gets the layer count.
        /// </summary>
        internal int LayerCount
        {
            get
            {
                return this.settings.Rasters.Count + this.settings.Vectors.Count;
            }
        }

        /// <summary>
        /// Gets the raster layer count.
        /// </summary>
        internal int RasterCount
        {
            get
            {
                return this.settings.Rasters.Count;
            }
        }

        /// <summary>
        /// Gets the list of raster layer paths.
        /// </summary>
        internal List<string> Rasters
        {
            get
            {
                return this.settings.Rasters;
            }
        }

        /// <summary>
        /// Gets or sets the display scale.
        /// </summary>
        internal double Scale
        {
            get
            {
                return this.settings.Scale;
            }

            set
            {
                this.settings.Scale = value;
            }
        }

        /// <summary>
        /// Gets the vector layer count.
        /// </summary>
        internal int VectorCount
        {
            get
            {
                return this.settings.Vectors.Count;
            }
        }

        /// <summary>
        /// Gets the list of vector layer paths.
        /// </summary>
        internal List<string> Vectors
        {
            get
            {
                return this.settings.Vectors;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the extension object.
        /// </summary>
        /// <returns>
        /// The <see cref="LinkMapsExtension" /> object.
        /// </returns>
        internal static LinkMapsExtension GetExtension()
        {
            if (extension == null)
            {
                UID extID = new UIDClass();
                extID.Value = ThisAddIn.IDs.LinkMapsExtension;
                ArcMap.Application.FindExtensionByCLSID(extID);
            }

            return extension;
        }

        /// <summary>
        /// Adds a raster layer.
        /// </summary>
        /// <param name="path">
        /// The path to the raster file.
        /// </param>
        internal void AddRaster(string path)
        {
            this.settings.Rasters.Add(path);
        }

        /// <summary>
        /// Adds a vector layer.
        /// </summary>
        /// <param name="path">
        /// The path to the vector file.
        /// </param>
        internal void AddVector(string path)
        {
            this.settings.Vectors.Add(path);
        }

        /// <summary>
        /// Clears the extension settings.
        /// </summary>
        internal void Clear()
        {
            this.settings.Clear();
        }

        /// <summary>
        /// Deletes a raster layer.
        /// </summary>
        /// <param name="path">
        /// The path the the raster file.
        /// </param>
        internal void DeleteRaster(string path)
        {
            this.settings.Rasters.Remove(path);
        }

        /// <summary>
        /// Deletes a vector layer.
        /// </summary>
        /// <param name="path">
        /// The path to the vector file.
        /// </param>
        internal void DeleteVector(string path)
        {
            this.settings.Vectors.Remove(path);
        }

        /// <summary>
        /// The OnLoad handler runs when an mxd is opened that contains serialized data from this
        /// extension. The formatter seems to be only able to serialize and deserialize a dictionary
        /// of strings correctly.
        /// </summary>
        /// <param name="inStrm">
        /// The input stream.
        /// </param>
        protected override void OnLoad(Stream inStrm)
        {
            try
            {
                var formatter = new BinaryFormatter();
                this.settings.Deserialize((Dictionary<string, string>)formatter.Deserialize(inStrm));
                Debug.Print("Loaded serialized data");
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    string.Format("Unable to load LinkMaps settings:\n{0}", ex.Message), 
                    "Error", 
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// The OnSave handler runs when the mxd is saved and saves the serialized extension data to
        /// the mxd. The formatter seems to be only able to serialize and deserialize a dictionary
        /// of strings correctly.
        /// </summary>
        /// <param name="outStrm">
        /// The output stream.
        /// </param>
        protected override void OnSave(Stream outStrm)
        {
            if (this.LayerCount > 0)
            {
                try
                {
                    var formatter = new BinaryFormatter();
                    formatter.Serialize(outStrm, this.settings.Serialize());
                    Debug.Print("Saved serialized data");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(
                        string.Format("Unable to save LinkMaps settings:\n{0}", ex.Message), 
                        "Error", 
                        MessageBoxButtons.OK, 
                        MessageBoxIcon.Error);
                }
            }
        }

        #endregion
    }
}