﻿// ----------------------------------------------------------------------------------------------------
// <copyright file="LinkMapsButton.cs" company="USU RS/GIS Laboratory">
//   Copyright (c) USU RS/GIS Laboratory. All rights reserved.
// </copyright>
// ----------------------------------------------------------------------------------------------------

namespace LinkMaps
{
    using ESRI.ArcGIS.Framework;

    using Button = ESRI.ArcGIS.Desktop.AddIns.Button;

    /// <summary>
    /// The button that opens the LinkMaps dockable window.
    /// </summary>
    public class LinkMapsButton : Button
    {
        #region Methods

        /// <summary>
        /// The OnClick handler opens the dockable window.
        /// </summary>
        protected override void OnClick()
        {
            IDockableWindow win = LinkMapsExtension.GetExtension().DockableWindow;
            win.Show(!win.IsVisible());
        }

        /// <summary>
        /// The OnUpdate handler enables the button if there is a valid ArcMap application object.
        /// </summary>
        protected override void OnUpdate()
        {
            this.Enabled = ArcMap.Application != null;
        }

        #endregion
    }
}